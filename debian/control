Source: libj2ssh-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Varun Hiremath <varunhiremath@gmail.com>,
           Emmanuel Bourg <ebourg@apache.org>
Section: java
Priority: optional
Build-Depends: ant,
               debhelper-compat (= 13),
               default-jdk-headless,
               javahelper,
               libcommons-logging-java,
               maven-repo-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/libj2ssh-java
Vcs-Git: https://salsa.debian.org/java-team/libj2ssh-java.git
Homepage: https://sshtools.sourceforge.net

Package: libj2ssh-java
Architecture: all
Depends: libcommons-logging-java,
         ${misc:Depends}
Description: Java library for the SSH protocol
 J2SSH is an object-orientated Java library implementation of the SSH
 version 2 protocol. It provides a rich, powerful, and extensible SSH
 API that enables developers to gain access to SSH servers and to
 develop entire SSH client/server frameworks. The API library provides
 a fully-featured SSH2 implementation specifically designed for
 cross-platform development. Higher level components, representing
 both the standard SSH client and SSH servers, are provided which
 implement the protocol specification for user sessions and port
 forwarding. The specification currently supports public key and
 password authentication and a full implementation of the SFTP
 protocol.

Package: libj2ssh-java-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: Java library for the SSH protocol - documentation and examples
 J2SSH is an object-orientated Java library implementation of the SSH
 version 2 protocol. It provides a rich, powerful, and extensible SSH
 API that enables developers to gain access to SSH servers and to
 develop entire SSH client/server frameworks. The API library provides
 a fully-featured SSH2 implementation specifically designed for
 cross-platform development. Higher level components, representing
 both the standard SSH client and SSH servers, are provided which
 implement the protocol specification for user sessions and port
 forwarding. The specification currently supports public key and
 password authentication and a full implementation of the SFTP
 protocol.
 .
 This package includes the API documentation as well as some examples
 for the j2ssh library.
