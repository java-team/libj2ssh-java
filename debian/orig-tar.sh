#!/bin/sh -e

# called by uscan with '--upstream-version' <version> <file>
TAR=../j2ssh-$2-src.tar.gz
DIR=libj2ssh-java-$2.orig

# clean up the upstream tarball
mv $TAR $3
tar zxf $3
mv j2ssh $DIR
GZIP=--best tar czf $3 --exclude 'lib/*' $DIR
rm -rf $TAR $DIR

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
    . .svn/deb-layout
    mv $TAR $origDir
    echo "moved $3 to $origDir"
fi

exit 0
